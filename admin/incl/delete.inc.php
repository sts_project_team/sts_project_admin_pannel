<?php

if (!@include_once("./incl/auth.inc.php"))
 include_once("../incl/auth.inc.php");

if (isset($_GET['directory_name'])) $directory_name = basename(stripslashes($_GET['directory_name']))."/";
if (isset($_GET['filename'])) $filename = basename(stripslashes($_GET['filename']));

if ($AllowDelete && isset($_GET['directory_name']) || $AllowDelete && isset($_GET['filename']))
{
 if (isset($_GET['delete']) && isset($_GET['directory_name']))
 {
  if ($_GET['directory_name'] == "../" || ($_GET['directory_name'] == "./"))
   print "<font color='#CC0000'>$StrFolderInvalidName</font>";
  else if (!file_exists($home_directory.$path.$directory_name))
   print "<font color='#CC0000'>$StrDeleteFolderNotFound</font>";
  else if (remove_directory($home_directory.$path.$directory_name) && @rmdir($home_directory.$path.$directory_name))
   print "<font color='#009900'>$StrDeleteFolderSuccess</font>";
  else
  {
   print "<font color='#CC0000'>$StrDeleteFolderFail</font><br /><br />";
   print $StrDeleteFolderFailHelp;
  }
 }

 else if (isset($_GET['delete']) && isset($_GET['filename']))
 {
  if ($_GET['filename'] == ".." || ($_GET['filename'] == "."))
   print "<font color='#CC0000'>$StrFileInvalidName</font>";
  else if (!file_exists($home_directory.$path.$filename))
   print "<font color='#CC0000'>$StrDeleteFileNotFound</font>";
  else if (@unlink($home_directory.$path.$filename))
   print "<font color='#009900'>$StrDeleteFileSuccess</font>";
  else
  {
   print "<font color='#CC0000'>$StrDeleteFileFail</font><br /><br />";
   print $StrDeleteFileFailHelp;
  }
 }

 else
 {
   print
   '
    <div id="delete" style = "width: 500px; border: 1px solid black;" class="dialog" title="Удалить файл">
		<div class="dialog-titlebar">
			';
			if (isset($_GET['directory_name'])) print "<span>$StrDeleteFolder \"".htmlentities(basename($directory_name))."\"?</span>";
			else if (isset($_GET['filename'])) print "<span>$StrDeleteFile \"".htmlentities($filename)."\"?</span>";
			print "<a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."'><img src='images/icons/back.gif' border=0 alt='$StrBack'></a>";
			print
			'
			<div class="dialog-close">
			</div>
		</div>
		<div class="dialog-content">
			<p class="dialog-title">Вы действительно хотите удалить файл?</p>
			';
			if (isset($_GET['directory_name']))
			 {
			  print "<p class=dialog-title'>$StrDeleteFolderQuestion /".htmlentities($path.$directory_name)."</p><br>";
			 }
			 else if (isset($_GET['filename']))
			 {
			  print "<p class=dialog-title'>$StrDeleteFileQuestion /".htmlentities($path.$filename)."</p><br>";
			 }
			 if (isset($_GET['directory_name'])) print "<a style = 'float: none;' href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;directory_name=".htmlentities(rawurlencode($directory_name))."&amp;output=delete&amp;delete=true'>$StrYes</a>";
			 else if (isset($_GET['filename'])) print "<a style = 'float: none;' href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;filename=".htmlentities(rawurlencode($filename))."&amp;output=delete&amp;delete=true'>$StrYes</a>";
			 print "<a style = 'float: none;' href='$base_url&amp;path=".htmlentities(rawurlencode($path))."'>$StrCancel</a>";
			print
			'
		</div>
	</div>
   ';
 }
}
else
 print "<font color='#CC0000'>$StrAccessDenied</font>";

?>