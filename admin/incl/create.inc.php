<?php

if (!@include_once("./incl/auth.inc.php"))
 include_once("../incl/auth.inc.php");

if ($AllowCreate && isset($_GET['create']) && isset($_POST['directory_name']))
{
 if (!is_valid_name(stripslashes($_POST['directory_name'])))
  print "<font color='#CC0000'>$StrFolderInvalidName</font>";
 else if (file_exists($home_directory.$path.stripslashes($_POST['directory_name']."/")))
  print "<font color='#CC0000'>$StrAlreadyExists</font>";
 else if (@mkdir($home_directory.$path.stripslashes($_POST['directory_name']), 0777))
  print "<font color='#009900'>$StrCreateFolderSuccess</font>";
 else
 {
  print "<font color='#CC0000'>$StrCreateFolderFail</font><br /><br />";
  print $StrCreateFolderFailHelp;
 }
}

else if ($AllowCreate && isset($_GET['create']) && isset($_POST['filename']))
{
 if (!is_valid_name(stripslashes($_POST['filename'])))
  print "<font color='#CC0000'>$StrFileInvalidName</font>";
 else if (file_exists($home_directory.$path.stripslashes($_POST['filename'])))
  print "<font color='#CC0000'>$StrAlreadyExists</font>";
 else if (@fopen($home_directory.$path.stripslashes($_POST['filename']), "w+"))
  print "<font color='#009900'>$StrCreateFileSuccess</font>";
 else
 {
  print "<font color='#CC0000'>$StrCreateFileFail</font><br /><br />";
  print $StrCreateFileFailHelp;
 }
}

else if ($AllowCreate)
{	
	print
	'
	<div id="add-dir" class="dialog" title="Создать директорию" style = "width: 500px; border: 1px solid black;">
		<div class="dialog-titlebar">
		';
		if ($_GET['type'] == "directory") print "<span>$StrCreateFolder</span>";
		else if ($_GET['type'] == "file") print "<span>$StrCreateFile</span>";
		print "<a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."'><img src='images/icons/back.gif' border=0 alt='$StrBack'></a>";
		print
		'
			<div class="dialog-close">
			</div>
		</div>
		<div class="dialog-content">
				';
				print "<form action='$base_url&amp;output=create&amp;create=true' method='post'>";
				if ($_GET['type'] == "directory") print "<p class='dialog-title'>$StrCreateFolderQuestion</p>";
				else if ($_GET['type'] == "file") print "<p class='dialog-title'>$StrCreateFileQuestion</p>";
				if ($_GET['type'] == "directory") print "<input type='text' name='directory_name'>&nbsp;<br>";
				else if ($_GET['type'] == "file") print "<input type='text' name='filename'>&nbsp;<br>";
				print "<input class='submit' type='submit' value='$StrCreate' style = 'float: none;'>";
				print "<input type='hidden' name=path value=\"".htmlentities($path)."\">";
				print "</form>";
				print
				'
		</div>
	</div>
	';
}
else
 print "<font color='#CC0000'>$StrAccessDenied</font>";

?>