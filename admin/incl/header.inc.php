<?php

list($seconds, $microseconds) = explode(" ", microtime());
$time_start = $seconds + $microseconds;

header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Content-Type: text/html; charset=utf-8");

@session_save_path($session_save_path);
error_reporting(E_ALL);
clearstatcache();
session_start();

if (((bool) ini_get('session.use_trans_sid')) == FALSE)
 $base_url = $_SERVER['PHP_SELF']."?".SID;
else
 $base_url = $_SERVER['PHP_SELF']."?";

if (isset($_POST['input_username']) && isset($_POST['input_password']) && $_POST['input_username'] == $username && md5($_POST['input_password']) == md5($password))
{
 $_SESSION['session_username'] = $_POST['input_username'];
 $_SESSION['session_password'] = md5($_POST['input_password']);
 header("Location: $base_url".SID);

}
else if (isset($_GET['action']) && $_GET['action'] == "logout")
{
 session_destroy();
 setcookie(session_name(),"",0,"/");
 header("Location: ".$_SERVER['PHP_SELF']);
}
?>