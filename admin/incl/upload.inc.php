<?php

if (!@include_once("./incl/auth.inc.php"))
 include_once("../incl/auth.inc.php");

if ($AllowUpload && isset($_GET['upload']))
{
 print "<table cellspacing=0 cellpadding=0 class='upload'>";

 if (!isset($_FILES['userfile']))
  // maximum post size reached
  print $StrUploadFailPost;
 else
 {
  for($i=0;$i<=count($_FILES['userfile']['name'])-1;$i++)
  {
   if (@move_uploaded_file($_FILES['userfile']['tmp_name'][$i], realpath($home_directory.$path)."/".$_FILES['userfile']['name'][$i]))
    print "<tr><td width='250'>$StrUploading ".$_FILES['userfile']['name'][$i]."</td><td width='50' align='center'>[<font color='#009900'>$StrUploadSuccess</font>]</td></tr>";
   else if ($_FILES['userfile']['name'][$i])
    print "<tr><td width='250'>$StrUploading ".$_FILES['userfile']['name'][$i]."</td><td width='50' align='center'>[<font color='#CC0000'>$StrUploadFail</font>]</td></tr>";
  }
 }
 print "</table>";
}

else if ($AllowUpload)
{
	print
	'
		<div id="upload-file" class="dialog" title="Загрузить файлы">
			<div class="dialog-titlebar">
				<span>'.$StrUploadFilesTo.' "/'.htmlentities($path).'"</span>
				';
				print "<a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."'><img src='images/icons/back.gif' border=0 alt='$StrBack'></a>";
				print '
				<div class="dialog-close">
				</div>
			</div>
			<div class="dialog-content">
			';
				print "<form action='$base_url&amp;output=upload&amp;upload=true' method='post' enctype='multipart/form-data'>";
				print "<input type='file' name='userfile[]' style = 'width: inherit;' multiple = 'true'>";
				print "<input class='submit' style = 'float: none;'  type='submit' value='$StrUpload'>";
				print "<input type='hidden' name=path value=\"".htmlentities($path)."\">";
				print "</form>";
			print
			'
			</div>
		</div>
	';
}
else
 print "<font color='#CC0000'>$StrAccessDenied</font>";

?>