<?php

if (!@include_once("./incl/auth.inc.php"))
 include_once("../incl/auth.inc.php");

if ($AllowEdit && isset($_GET['save']) && isset($_POST['filename']))
{
 $text = $_POST['text'];

 if (!is_valid_name(stripslashes($_POST['filename'])))
  print "<font color='#CC0000'>$StrFileInvalidName</font>";
 else if ($fp = @fopen ($home_directory.$path.stripslashes($_POST['filename']), "wb"))
 {
  fwrite($fp, $text);
  fclose($fp);
  print "<font color='#009900'>$StrSaveFileSuccess</font>";
 }
 else
  print "<font color='#CC0000'>$StrSaveFileFail</font>";

}

else if ($AllowEdit && isset($_GET['filename']))
{
 $filename = basename(stripslashes($_GET['filename']));
 print
 '
	<div id="edit" style = "width: 800px; border: 1px solid black;" class="dialog" title="Редактирование">
		<div class="dialog-titlebar">
			';
			print "<span>$StrEditing \"".htmlentities($filename)."\"</span>";
			print
			'
			<div class="dialog-close">
			</div>
		</div>
		<div class="dialog-content">
			';
	if ($fp = @fopen($home_directory.$path.$filename, "rb"))
   {
    print "<form action='$base_url&amp;output=edit&amp;save=true' method='post'>";

    print "<textarea cols=120 rows=20 name='text'>";
    print htmlentities(fread($fp, filesize($home_directory.$path.$filename)));
    fclose ($fp);
    print "</textarea>";

    print "<br /><br />";
    print "$StrFilename <input size=40 name='filename' value=\"".htmlentities($filename)."\">";

    print "<br /><br />";
   print "<input class='bigbutton' style = 'float: none; width: 125px;' type='reset' value='$StrRestoreOriginal'>&nbsp;<input class='bigbutton' style = 'float: none;' type='submit' value='$StrSaveAndExit'>";

   print "<input type='hidden' name='path' value=\"".htmlentities($path)."\">";
   print "</form>";
   }
			print
			'
		</div>
	</div>
 ';
}
else
 print "<font color='#CC0000'>$StrAccessDenied</font>";

?>