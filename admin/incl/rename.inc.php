<?php

if (!@include_once("./incl/auth.inc.php"))
 include_once("../incl/auth.inc.php");

if ($AllowRename && isset($_GET['directory_name']) || $AllowRename && isset($_GET['filename']) || $AllowRename && isset($_POST['directory_name']) || $AllowRename && isset($_POST['filename']))
{
 if (isset($_GET['directory_name'])) $directory_name = basename(stripslashes($_GET['directory_name']))."/";
 if (isset($_GET['filename'])) $filename = basename(stripslashes($_GET['filename']));
 if (isset($_POST['directory_name'])) $directory_name = basename(stripslashes($_POST['directory_name']))."/";
 if (isset($_POST['filename'])) $filename = basename(stripslashes($_POST['filename']));
 if (isset($_POST['new_directory_name'])) $new_directory_name = basename(stripslashes($_POST['new_directory_name']))."/";
 if (isset($_POST['new_filename'])) $new_filename = basename(stripslashes($_POST['new_filename']));

 if (isset($_GET['rename']) && isset($_POST['directory_name']))
 {
  if (!is_valid_name(substr($new_directory_name, 0, -1)))
   print "<font color='#CC0000'>$StrFolderInvalidName</font>";
  else if (@file_exists($home_directory.$path.$new_directory_name))
   print "<font color='#CC0000'>$StrAlreadyExists</font>";
  else if (@rename($home_directory.$path.$directory_name, $home_directory.$path.$new_directory_name))
   print "<font color='#009900'>$StrRenameFolderSuccess</font>";
  else
  {
   print "<font color='#CC0000'>$StrRenameFolderFail</font><br /><br />";
   print $StrRenameFolderFailHelp;
  }
 }

 else if (isset($_GET['rename']) && isset($_POST['filename']))
 {
  if (!is_valid_name($new_filename))
   print "<font color='#CC0000'>$StrFileInvalidName</font>";
  else if (@file_exists($home_directory.$path.$new_filename))
   print "<font color='#CC0000'>$StrAlreadyExists</font>";
  else if (@rename($home_directory.$path.$filename, $home_directory.$path.$new_filename))
   print "<font color='#009900'>$StrRenameFileSuccess</font>";
  else
  {
   print "<font color='#CC0000'>$StrRenameFileFail</font><br /><br />";
   print $StrRenameFileFailHelp;
  }
 }

 else
 {
  print
  '
	<div id="rename" style = "width: 500px; border: 1px solid black;" class="dialog" title="Переименовать файл">
		<div class="dialog-titlebar">
			';
			if (isset($_GET['directory_name'])) print "<span>$StrRenameFolder \"".htmlentities(basename($directory_name))."\"</span>";
			else if (isset($_GET['filename'])) print "<span>$StrRenameFile \"".htmlentities($filename)."\"</span>";
			print "<a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."'><img src='images/icons/back.gif' border=0 alt='$StrBack'></a>";
			print
			'
			<div class="dialog-close">
			</div>
		</div>
		<div class="dialog-content">
			';
			if (isset($_GET['directory_name'])) print "<p class='dialog-title'>$StrRenameFolderQuestion</p>";
			else if (isset($_GET['filename'])) print "<p class='dialog-title'>$StrRenameFileQuestion</p>";
			print "<form action='$base_url&amp;output=rename&amp;rename=true' method='post'>";
			if (isset($_GET['directory_name'])) print "<input name='new_directory_name' value=\"".htmlentities(basename($directory_name))."\" size=40>&nbsp;";
			else if (isset($_GET['filename'])) print "<input name='new_filename' value=\"".htmlentities($filename)."\" size=40>&nbsp;";
			print "<input class='bigbutton' style = 'width: 140px;' type='submit' value='$StrRename'>";
			if (isset($_GET['directory_name'])) print "<input type='hidden' name=directory_name value=\"".htmlentities($directory_name)."\">";
			else if (isset($_GET['filename'])) print "<input type='hidden' name=filename value=\"".htmlentities($filename)."\">";
			print "<input type='hidden' name=path value=\"".htmlentities($path)."\">";
			print "</form>";
			print
			'
		</div>
	</div>
  ';
 }
}
else
 print "<font color='#CC0000'>$StrAccessDenied</font>";

?>