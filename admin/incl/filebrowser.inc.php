<?php
	if (!@include_once("./incl/auth.inc.php"))
	include_once("../incl/auth.inc.php");
	print '<div class = "wrapper">';
	print '<div class = "admin-panel">';
	if (!isset($_GET['sortby'])) $_GET['sortby']	= "filename";
	if (!isset($_GET['order'])) $_GET['order']	= "asc";

	print
	'
		<div id="header">
               <div class="inner">
                    <ul class="admin-menu">
                        <li><a class="add-dir" href="'.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'&amp;action=create&amp;type=directory">Создать директорию</a></li>
                        <li><a class="add-file" href="'.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'&amp;action=create&amp;type=file">Создать файл</a></li>
                        <li><a class="upload-file" href="'.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'&amp;action=upload">Загрузить файлы на сервер</a></li>
                        <li><a class="logout" href="'.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'&amp;action=logout">Выйти</a></li>
                    </ul>
               </div>
            </div>
	';
	if ($open = @opendir($home_directory.$path))
	{
		for($i=0;($directory = readdir($open)) != FALSE;$i++)
		if (is_dir($home_directory.$path.$directory) && $directory != "." && $directory != ".." && !is_hidden_directory($home_directory.$path.$directory))
		$directories[$i] = array($directory,$directory);
		closedir($open);

		if (isset($directories))
		{
			sort($directories);
			reset($directories);
		}
	}
	print
	'
	<div class="admin-main">
		<div class="inner">
			<div class="admin-panel-title">Содержание</div>
			<div class="admin-panel-content">
				<div class="admin-content-left">
					<table>
						<tr>
							<th></th>
							<th class="name">Имя</th>
							<th>Имя</th>
							<th>Удал</th>
						</tr>
						<tr>
							<td class="folder"><a href="'.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'"></a></td>
							<td><a href='.$base_url.'>.</a></td>
							<td class="rename"></td>
							<td class="delete"></td>
						</tr>
						<tr>
							<td class="folder"><a href='.$base_url.'&amp;path='.htmlentities(rawurlencode(dirname($path))).'/></a></td>
							<td><a href='.$base_url.'&amp;path='.htmlentities(rawurlencode(dirname($path))).'/>..</a></td>
							<td class="rename"></td>
							<td class="delete"></td>
						</tr>
						';
						if (isset($directories)) foreach($directories as $directory)
						{
							print
							'
							<tr>
								<td class="folder"><a href="'.$base_url.'&amp;path="'.htmlentities(rawurlencode($path.$directory[0])).'"/"></a></td>
								<td><a href="'.$base_url.'&amp;path='.htmlentities(rawurlencode($path.$directory[0])).'/">'.htmlentities($directory[0]).'</a></td>
							';
							if ($AllowRename) print "<td class = 'rename'><a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;directory_name=".htmlentities(rawurlencode($directory[0]))."/&amp;action=rename'></a></td>";
							if ($AllowDelete) print "<td class = 'delete'><a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;directory_name=".htmlentities(rawurlencode($directory[0]))."/&amp;action=delete'></a></td>";
							print 
							'	
							</tr>';
						}
					print '
					</table>
				</div>';
				if ($open = @opendir($home_directory.$path))
				{
					for($i=0;($file = readdir($open)) != FALSE;$i++)
						if (is_file($home_directory.$path.$file) && !is_hidden_file($home_directory.$path.$file))
						{
							//$icon = get_icon($file);
							$filesize = filesize($home_directory.$path.$file);
							$permissions = decoct(fileperms($home_directory.$path.$file)%01000);
							$modified = filemtime($home_directory.$path.$file);
							$extension = "";
							$files[$i] = array(
								//"icon"        => $icon,
								"filename"    => $file,
								"filesize"    => $filesize,
								"permissions" => $permissions,
								"modified"    => $modified,
								"extension"   => $extension,
								);
						}
						closedir($open);

					if (isset($files))
					{
						usort($files, "compare_filedata");
						reset($files);
					}
				}

				print
				'
				<div class="admin-content-right">
					<table>
						<tr>
							<th></th>
							<th class="name"><a href='.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'&amp;sortby=filename&amp;order='.get_opposite_order("filename", $_GET['order']).'>Имя файла</a></th>
							<th><a href='.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'&amp;sortby=filesize&amp;order='.get_opposite_order("filesize", $_GET['order']).'>Размер</a></th>
							<th><a href='.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'&amp;sortby=permissions&amp;order='.get_opposite_order("permissions", $_GET['order']).'>Права</a></th>
							<th><a href='.$base_url.'&amp;path='.htmlentities(rawurlencode($path)).'&amp;sortby=modified&amp;order='.get_opposite_order("modified", $_GET['order']).'>Изменён</a></th>
							<th>Просм</th>
							<th class="edit">Ред</th>
							<th class="rename">Имя</th>
							<th class="download">Ск</th>
							<th class="delete">Удал</th>
							<th>Ссылка на файл</th>
						</tr>
						';
						  if (isset($files)) foreach($files as $file)
						  {
						   $file['filesize'] = get_better_filesize($file['filesize']);
						   $file['modified'] = date($ModifiedFormat, $file['modified']);
						print
						'
						<tr>
							<td class="document"><span></span></td>
							<td class="name">'.htmlentities($file['filename']).'</td>
							<td>'.$file['filesize'].'</td>
							<td>'.$file['permissions'].'</td>
							<td>'.$file['modified'].'</td>
							';
							if ($AllowView && is_viewable_file($file['filename'])) 
								print "<td><a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;filename=".htmlentities(rawurlencode($file['filename']))."&amp;action=view&amp;size=100'><img src='images/icons/view.gif' width=20 height=22></a></td>";
								else if ($AllowEdit) 
									print "<td></td>";
							if ($AllowEdit && is_editable_file($file['filename'])) 
								print "<td class= 'edit'><a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;filename=".htmlentities(rawurlencode($file['filename']))."&amp;action=edit'></a></td>";
								else if ($AllowEdit) 
									print "<td></td>";
							if ($AllowRename) 
								print "<td  class= 'rename'><a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;filename=".htmlentities(rawurlencode($file['filename']))."&amp;action=rename'></a></td>";
							if ($AllowDownload) 
								print "<td class = 'download'><a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;filename=".htmlentities(rawurlencode($file['filename']))."&amp;action=download'></a></td>";
							if ($AllowEdit) 
								print "<td class = 'delete'><a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."&amp;filename=".htmlentities(rawurlencode($file['filename']))."&amp;action=delete'></a></td>";
							if(htmlentities($path) == "")
								$str_path = "admin/"; 
								else
									$str_path = htmlentities($path);
							if ($AllowView && is_viewable_file($file['filename'])) 
								print '<td><input class = "url" value = http://'.$_SERVER['HTTP_HOST'].'/'.$str_path.htmlentities(rawurlencode($file['filename'])).'></td>';
						}
						print
						'
					</table>
				</div>
			</div>
		</div>
	</div>
	';
	print "</div>";
	print "</div>";
?>