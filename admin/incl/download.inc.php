<?php

if (!@include_once("./incl/auth.inc.php"))
 include_once("../incl/auth.inc.php");

if (isset($_GET['filename'])) $filename = stripslashes($_GET['filename']);

if ($AllowDownload)
{
  print
  '
	<div id="download" style = "width: 500px; border: 1px solid black;" class="dialog" title="Скачать файл">
		<div class="dialog-titlebar">
			<span>Скачать файл "'.htmlentities($filename).'"</span>
			';
			print "<a href='$base_url&amp;path=".htmlentities(rawurlencode($path))."'><img src='images/icons/back.gif' border=0 alt='$StrBack'></a>";
			print
			'
			<div class="dialog-close">
			</div>
		</div>
		<div class="dialog-content">
			<p class="dialog-title">Нажмите на кнопку чтобы начать загрузку.</p>
			';
			print "<a style = 'float: none' href='incl/libfile.php?path=".htmlentities(rawurlencode($path))."&amp;filename=".htmlentities(rawurlencode($filename))."&amp;action=download'>Скачать</a>";
			print
			'
		</div>
	</div>
  ';
}
else
 print "<font color='#CC0000'>$StrAccessDenied</font>";

?>