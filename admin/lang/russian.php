<?php

/*$StrFolderNameShort		= "Имя";
$StrFileNameShort		= "Имя файла";
$StrFileSizeShort		= "Размер";
$StrPermissionsShort		= "Права";
$StrLastModifiedShort		= "Изменен";
$StrEditShort			= "Ред";
$StrViewShort			= "Просм";
$StrRenameShort			= "Имя";
$StrDownloadShort		= "Ск";
$StrDeleteShort			= "Удал";*/

$StrBack			= "Назад";
$StrYes				= "Да";
$StrOr				= "или";
$StrCancel			= "Отмена";

$StrOpenFolder			= "Открыть папку";
$StrRenameFolder		= "Переименовать папку";
$StrDeleteFolder		= "Удалить папку";

$StrViewFile			= "Просмотр файла";
$StrEditFile			= "Редактировать файл";
$StrRenameFile			= "Переименовать";
$StrDownloadFile		= "Сгрузить файл";
$StrDeleteFile			= "Удалить файл";

$StrUsername			= "Логин:";
$StrPassword			= "Пароль:";
$StrLogIn			= "Войти";
$StrLoginSystem			= "Вход в систему:";
$StrLoginInfo			= "Введите, пожалуйста, логин и пароль:";

$StrAccessDenied		= "Доступ закрыт!";

$StrInvalidHomeFolder		= "Ошибочная домашняя директория.";
$StrInvalidPath			= "Ошибочный путь.";
$StrMissingTrailingSlash	= "(Отсутствует слэш в пути)";
$StrAlreadyExists		= "Такой файл или директория уже существует.";
$StrFolderInvalidName		= "Неправильное имя папки.";
$StrFileInvalidName		= "Неправильное имя файла.";
$StrErrorOpeningFile		= "Ошибка открытия файла.";

$StrSaveFileSuccess		= "Файл сохранен!";
$StrSaveFileFail		= "Ошибка сохранения файла.";
$StrEditing			= "Редактирование";
$StrFilename			= "Имя файла:";
$StrRestoreOriginal		= "Восстановить";
$StrSaveAndExit			= "Сохранить";

$StrDeleteFolderNotFound	= "Папка не найдена.";
$StrDeleteFileNotFound		= "Файл не найден.";
$StrDeleteFolderSuccess		= "Папка удалена!";
$StrDeleteFileSuccess		= "Файл удален!";
$StrDeleteFolderFail		= "Ошибка удаления папки.";
$StrDeleteFileFail		= "Ошибка удаления файла.";
$StrDeleteFolderFailHelp	= "Возможно всё дело в ваших правах на папку.";
$StrDeleteFileFailHelp		= "Возможно всё дело в ваших правах на файл.";
$StrDeleteFolderQuestion	= "Вы действительно хотите удалить папку?";
$StrDeleteFileQuestion		= "Вы действительно хотите удалить файл?";

$StrRename			= "Переименовать";
$StrRenameFolder		= "Переименовать папку";
$StrRenameFile			= "Переименовать файл";
$StrRenameFolderSuccess		= "Папка переименована!";
$StrRenameFileSuccess		= "Файл переименован!";
$StrRenameFolderFail		= "Не удалось переименовать папку.";
$StrRenameFileFail		= "Не удалось переименовать файл.";
$StrRenameFolderFailHelp	= "Возможно всё дело в ваших правах на папку или некорректном имени.";
$StrRenameFileFailHelp		= "Возможно всё дело в ваших правах на файл или некорректном имени.";
$StrRenameFolderQuestion	= "Выберите новое имя для следующей папки:";
$StrRenameFileQuestion		= "Выберите новое имя для следующего файла:";

$StrCreate			= "Создать";
$StrCreateFolder		= "Создать папку";
$StrCreateFile			= "Создать файл";
$StrCreateFolderSuccess		= "Папка создана!";
$StrCreateFileSuccess		= "Файл создан!";
$StrCreateFolderFail		= "Ошибка создания папки.";
$StrCreateFileFail		= "Ошибка создания файла.";
$StrCreateFolderFailHelp	= "Возможно всё дело в ваших правах.";
$StrCreateFileFailHelp		= "Возможно всё дело в ваших правах.";
$StrCreateFolderQuestion	= "Выберите  имя для новой папки:";
$StrCreateFileQuestion		= "Выберите  имя для нового файла:";

$StrUpload			= "Загрузить на сервер";
$StrUploadFilesTo		= "Загрузить файлы в";
$StrUploading			= "Загрузка";
$StrUploadSuccess		= "OK!";
$StrUploadFail			= "Ошибка!";
$StrUploadFailPost		= "Слишком большой размер файла / смотрите faq.txt";
$StrFirstFile			= "1-й файл:";
$StrSecondFile			= "2-й файл:";
$StrThirdFile			= "3-й файл:";
$StrFourthFile			= "4-й файл:";
$StrUploadQuestion		= "Выберите файлы которые вы хотите загрузить:";
$StrUploadNote			= "Внимание: Файлы будут помещены в:";

$StrDownload			= "Сгрузить";
$StrDownloadClickLink		= "Нажмите ссылку чтобы начать загрузку.";
$StrDownloadClickHere		= "нажать здесь для загрузки";
$StrDownloadFail		= "Ошибка открытия файла или неправильное имя.";

$StrViewing			= "Просмотр";
$StrAt				= "в";
$StrViewFail			= "Ошибка открытия картинки.";
$StrViewFailHelp		= "Файл не существует или это не картинка.";
$StrImage			= "Картинка";
$StrZoomIn			= "Ближе";
$StrZoomOut			= "Дальше";
$StrOriginalSize		= "Оригинальный размер";
$StrPrevious			= "Предыдущий";
$StrNext			= "Следующий";

?>