$(document).ready(function () {

    //feedback select customize
    $(".feedback-select").select2({
        placeholder: "Выберите из списка"
    });

    //program page hover program
    $(".program-block").mouseover(function(){
        $(this).addClass("hover");
    });
    $(".program-block").mouseout(function(){
        $(this).removeClass("hover");
    });


    //home-page slider
    var HomeSlider = new Swiper('.swiper-container',{
        mode:'horizontal',
        autoplay: 5000,
        speed: 500,
        calculateHeight:true,
        simulateTouch:false,
        loop:true
    });
    $('.arrow-left').click(function(e){HomeSlider.swipePrev()});
    $('.arrow-right').click(function(e){HomeSlider.swipeNext()});

    //home-page list of projects toggle
    $(".more-projects").click(function(){
        $(".small").toggleClass("show");
        $(".large").toggleClass("show");
        $(".home-projects-content").toggleClass("no-back");
    });

});